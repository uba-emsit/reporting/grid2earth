import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Logger;

/**
 * Converter for UNECE gridded emission data to Google Earth Viewer
 * 
 * @author Kevin Hausmann
 */
public class Converter {

	/** Path to source folder */
	private static final String SOURCE_FILE = "2005_gerastert.csv";

	/** Path to result file, will be created/overwritten. */
	private static final String RESULT_FILE = "result.kml";
	
	/** Separation character in input tables. */
	private static final String INPUT_SEPARATOR = ";";
	
	/** Source file column pollutant is listed in. */ 
	private static final int COLUMN_OF_POLLUTANT = 4;
	
	/** Maximum value for pollutant listed, used for categorisation. */
	private static final int MAX_VALUE = 50;
	
	/** Factor for the bar altitude, is multiplied with pollutant value and used as height in meters. */
	private static final int ALTITUDE_FACTOR = 1000;
		
	/** Set to true, if you want a clickable Point Placemark on top of the bar. */
	private static final boolean ADD_PIN = false;
	
	private static Logger logger = Logger.getLogger("");
	
	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		double maxValue = 0;
		File sourceFile = new File(SOURCE_FILE);
		File resultFile = new File(RESULT_FILE);
		
		logger.info("Reading: " + sourceFile.getAbsolutePath());
		
		// Clear result file 
		if (resultFile.exists()) {
			resultFile.delete();
			resultFile.createNewFile();
		}
		
		BufferedReader reader = new BufferedReader(new FileReader(sourceFile));
		
		logger.info("Country " + reader.readLine().split(INPUT_SEPARATOR)[1] + " found.");
		reader.readLine();
		
		short year = Short.parseShort(reader.readLine().split(INPUT_SEPARATOR)[1]);
		logger.info("Year " + year + " found.");
		
		String pollutant = reader.readLine().split(INPUT_SEPARATOR)[COLUMN_OF_POLLUTANT];
		logger.info("Reading column labeled: " + pollutant);
		
		String unit = reader.readLine().split(INPUT_SEPARATOR)[COLUMN_OF_POLLUTANT];
		logger.info("Column uses unit: " + unit);
						
		BufferedWriter writer = new BufferedWriter(new FileWriter(resultFile));
		writePreamble(writer);
		writeLine(writer, "<name> Gerasterte " + pollutant + " Emissionen " + year + "</name>");
		
		addStyle(writer, "lowest", "008500");
		addStyle(writer, "lower", "00bf50");
		addStyle(writer, "low", "0ad2d9");
		addStyle(writer, "medium", "0a8ef3");
		addStyle(writer, "high", "0505e8");
			    		
		String line = reader.readLine();
		while (line != null) {
			int iCell = Integer.valueOf(line.split(INPUT_SEPARATOR)[1]);
			int jCell = Integer.valueOf(line.split(INPUT_SEPARATOR)[2]);
			double value = Double.valueOf(line.split(INPUT_SEPARATOR)[COLUMN_OF_POLLUTANT].replace(',', '.'));
			
			if (value > maxValue) maxValue = value;
			
			Coordinate center = cellIndexToCoordinate(iCell, jCell);
			GridCellPolygon shape = cellToPolygon(iCell, jCell);
			int altitude = (int)(value * ALTITUDE_FACTOR);
			
			writeLine(writer, "<Placemark>");
			writeLine(writer, "<name>Zelle: " + iCell + "/" + jCell + "</name>");
			writeLine(writer, "<description>Emissionswert: " + Math.floor(value*100)/100 + " " 
					+ unit + " " + pollutant + "</description>");
			
			if (value <= MAX_VALUE * 1/20) writeLine(writer, "<styleUrl>#lowest</styleUrl>");
			else if (value <= MAX_VALUE * 1/10) writeLine(writer, "<styleUrl>#lower</styleUrl>");
			else if (value <= MAX_VALUE * 1/5) writeLine(writer, "<styleUrl>#low</styleUrl>");
			else if (value <= MAX_VALUE * 1/2) writeLine(writer, "<styleUrl>#medium</styleUrl>");
			else writeLine(writer, "<styleUrl>#high</styleUrl>");
						
			if (ADD_PIN) {
				writeLine(writer, "<MultiGeometry>");
				writeLine(writer, "<Point>");
				writeLine(writer, "<altitudeMode>relativeToGround</altitudeMode>");
				writeLine(writer, "<coordinates>");
				writeLine(writer, center.longitude + ", " + center.latitude + ", " + altitude);
				writeLine(writer, "</coordinates>");
			    writeLine(writer, "</Point>");
			}
			
			writeLine(writer, "<Polygon>");
			writeLine(writer, "<extrude>1</extrude>");
			writeLine(writer, "<altitudeMode>relativeToGround</altitudeMode>");
			writeLine(writer, "<outerBoundaryIs>");
			writeLine(writer, "<LinearRing>");
			writeLine(writer, "<coordinates>");
			
			
			writeLine(writer, shape.south + "," + altitude + " " +
					shape.east + "," + altitude + " " + shape.north + "," + altitude + " " +
					shape.west + "," + altitude + " " + shape.south + "," + altitude);
			
			writeLine(writer, "</coordinates>");
			writeLine(writer, "</LinearRing>");
			writeLine(writer, "</outerBoundaryIs>");
			writeLine(writer, "</Polygon>");
			
			if (ADD_PIN) writeLine(writer, "</MultiGeometry>");
			
			writeLine(writer, "</Placemark>");
			line = reader.readLine();
		}
		
		
		writeFinish(writer);
		reader.close();
		writer.close();
		
		logger.info("Result file " + resultFile.getAbsolutePath() + " closed.\nMax value was " + maxValue);
	}
	
	private static void addStyle(BufferedWriter writer, String id, String colorCode) throws IOException {
		writeLine(writer, "<Style id=\"" + id + "\">");
		
		if (ADD_PIN){
			writeLine(writer, "<LabelStyle>");
			writeLine(writer, "<scale>0</scale>");
			writeLine(writer, "</LabelStyle>");
			writeLine(writer, "<IconStyle>");
			writeLine(writer, "<color>ff" + colorCode + "</color>");
			writeLine(writer, "<scale>0.5</scale>");
			writeLine(writer, "</IconStyle>");
		}
		
		writeLine(writer, "<LineStyle>");
		writeLine(writer, "<color>ff" + colorCode + "</color>");
		writeLine(writer, "<width>1</width>"); 
		writeLine(writer, "</LineStyle>");
		writeLine(writer, "<PolyStyle>");
		writeLine(writer, "<color>bf" + colorCode + "</color>");
		writeLine(writer, "<colorMode>normal</colorMode>"); 
		writeLine(writer, "<fill>1</fill>");
		writeLine(writer, "<outline>1</outline>");
		writeLine(writer, "</PolyStyle>");
		writeLine(writer, "</Style>");
	}

	public static GridCellPolygon cellToPolygon(int iCell, int jCell) {
		GridCellPolygon shape = new GridCellPolygon();
		
		shape.north = cellIndexToCoordinate(iCell - 0.5, jCell + 0.5);
		shape.south = cellIndexToCoordinate(iCell + 0.5, jCell - 0.5);
		shape.east = cellIndexToCoordinate(iCell + 0.5, jCell + 0.5);
		shape.west = cellIndexToCoordinate(iCell - 0.5, jCell - 0.5);
		
		return shape;
	}
	
	/* These are constants used in method below. */
	private static final int xpol = 8;
	private static final int ypol = 110;
	private static final double M = 237.73d;
	private static final int lambdaZero = -32;
	
	public static Coordinate cellIndexToCoordinate(double iCell, double jCell) {
		Coordinate coordinate = new Coordinate();
		
		coordinate.latitude = 90 - (360/Math.PI) * Math.atan2(Math.sqrt(Math.pow(iCell - xpol, 2) + Math.pow(jCell - ypol, 2)), M);
		coordinate.longitude = lambdaZero + (180/Math.PI) * Math.atan2(iCell - xpol, ypol - jCell);
		
		return coordinate;
	}
	
	private static void writePreamble(BufferedWriter writer) throws IOException {
		writeLine(writer, "<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		writeLine(writer, "<kml xmlns=\"http://earth.google.com/kml/2.2\">");
		writeLine(writer, "<Document>");
	}
	
	private static void writeFinish(BufferedWriter writer) throws IOException {
		writeLine(writer, "</Document>");
		writeLine(writer, "</kml>");
	}
	
	private static void writeLine(BufferedWriter writer, String line) throws IOException {
		writer.write(line + "\n");
	}
}

class GridCellPolygon {
	public Coordinate north;
	public Coordinate south;
	public Coordinate east;
	public Coordinate west;
}

class Coordinate {
	
	public double longitude;
	public double latitude;
	
	public Coordinate() {
	}
	
	public Coordinate(double longitude, double latitude) {
		this.longitude = longitude;
		this.latitude = latitude;
	}
	
	@Override
	public String toString() {
		return longitude + "," + latitude;
	}
}
